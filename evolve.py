#!/usr/bin/env python

from collections.abc import Sequence
import pickle
from functools import partial
import sys

import jax
import jax.numpy as jnp

import flax
import flax.linen as nn
import flax.optim as optim

import eh

class FLRWAnsatz(nn.Module):
    """
    Ansatz designed to converge to FLRW metrics.
    """
    @nn.compact
    def __call__(self, x):
        pass

class MLPAnsatz(nn.Module):
    """
    Generic MLP-based ansatz.
    """
    @nn.compact
    def __call__(self, x):
        pass
