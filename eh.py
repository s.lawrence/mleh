#!/usr/bin/env python

from functools import partial

import jax
import jax.numpy as jnp

# Normalization on the EH action.
L0 = 1.

def ricci(g, dg, ddg):
    R = jnp.zeros(g.shape)

    R += -1/2 * jnp.einsum('ijab,ab->ij', ddg, g)
    R += -1/2 * jnp.einsum('abij,ab->ij', ddg, g)
    R += -1/2 * jnp.einsum('ibja,ab->ij', ddg, g)
    R += -1/2 * jnp.einsum('jbia,ab->ij', ddg, g)

    R += 1/2 * jnp.einsum('aci,bdj,ab,cd->ij', dg, dg, g, g)
    R += 1/2 * jnp.einsum('ica,jdb,ab,cd->ij', dg, dg, g, g)
    R += 1/2 * jnp.einsum('ica,jbd,ab,cd->ij', dg, dg, g, g)

    A = -dg
    A += jnp.einsum('ijc->cij',dg)
    A += jnp.einsum('ijc->icj',dg)
    B = -dg
    B += 2*jnp.einsum('abd->dab',dg)
    R -= 1/4 * jnp.einsum('ijc,abd,ab,cd', A, B, g, g)
    return R

@partial(jax.vmap, in_axes=(0,0,0,0,None), out_axes=0)
def lagrangian(x, g, dg, ddg, Lambda):
    """
    Strictly speaking, this is the Lagrange density.
    """
    R = jnp.trace(ricci(g, dg, ddg))
    return L0 * (jnp.sqrt(-jnp.linalg.det(g)) * (R-2*Lambda))

def eom():
    pass
