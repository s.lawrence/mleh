#!/usr/bin/env python

from collections.abc import Sequence
import pickle
from functools import partial
import sys

import numpy as np

import jax
import jax.numpy as jnp

import flax
import flax.linen as nn
import flax.optim as optim

import eh

Lambda = float(sys.argv[1])

class DiagonalAnsatz(nn.Module):
    """
    A static ansatz designed to converge to flat spacetime.
    """
    @nn.compact
    def __call__(self, x):
        return jnp.diag(jnp.array([-1., 1., 1., 1.]))

class SchwarzschildAnsatz(nn.Module):
    """
    A static ansatz designed to converge to the Schwarzschild metric ---
    most likely in isotropic coordinates.
    """
    @nn.compact
    def __call__(self, x):
        t, x = x[...,0], x[...,1:]
        params = self.param('params', nn.initializers.ones, (8,))
        r = jnp.sqrt(jnp.sum(x*x))
        gtt = - ((params[0] + params[1]*r) / (params[2]+params[3]*r)) ** (-params[4])
        gxx = (params[5] + params[6] * r) ** (-params[7])
        return jnp.diag(jnp.array([gtt,gxx,gxx,gxx]))

class KerrAnsatz(nn.Module):
    @nn.compact
    def __call__(self, x):
        pass

class StationaryMLPAnsatz(nn.Module):
    """
    MLP-based ansatz that assumes only a stationary spacetime. (The metric is
    independent of the time coordinate.)
    """
    features: Sequence[int]

    @nn.compact
    def __call__(self, x):
        pass

@partial(jax.vmap, in_axes=(None,None,0), out_axes=0)
def metric_derivatives(m, p, x):
    f = lambda y: m.apply(p, y)
    g = f(x)
    dg = jax.jacfwd(f)(x)
    ddg = jax.jacfwd(jax.jacfwd(f))(x)
    return g, dg, ddg

key = jax.random.PRNGKey(0)
ansatz = DiagonalAnsatz()
params = ansatz.init(key, jnp.zeros((3,)))

BATCH = 100

# Get random points.
r = np.random.standard_cauchy(size=(BATCH,))
d = np.random.normal(size=(4,BATCH))
d[0,:] = 0
x = d * r / np.sqrt(np.sum(d*d,axis=0))
x = jnp.transpose(x)

# Obtain the metric and its derivatives.
g, dg, ddg = metric_derivatives(ansatz, params, x)

# Get the lagrangian.
lagrangian = eh.lagrangian(x, g, dg, ddg, Lambda)
print(lagrangian)

# TODO really we want to look at the variation of the lagrangian with respect to the metric parameters...

